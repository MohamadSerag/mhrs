from os.path import abspath
from traceback import format_exc
from hmac import compare_digest
from hashlib import sha256

from PyQt5.QtSql import QSqlDatabase
from PyQt5.QtWidgets import *

from core import *
from models import make_act_model, make_proj_cat_model, make_dept_weight_model, \
    make_act_change_history_model
from views import CredentialsView, TextDisplay, ActEditorView, ActHistoryView
from toolkit import HyperWorkbook
from toolkit import cfg
import db


class Controller(metaclass=ABCMeta):
    def __init__(self, view=None, model=None):
        self._view = view
        self._model = model


class ActEditorController:
    def __init__(self, view=None, models=None):
        self._view = view
        self._act_model = models[0].sourceModel()
        self._act_proxy_model = models[0]
        self._proj_cat_model = models[1]
        self._dept_weight_model = models[2]
        self._models = [self._act_model, self._proj_cat_model, self._dept_weight_model]

    def _filter_detail_models(self, index):
        if any(model.isDirty() for model in [self._proj_cat_model, self._dept_weight_model]):
            res = QMessageBox.warning(
                self._view,
                'Warning',
                "Changes detected in the detail models, do you want to save?",
                QMessageBox.Save | QMessageBox.No
            )
            if res == QMessageBox.Save:
                for model in [self._proj_cat_model, self._dept_weight_model]:
                    model.submitAll()

        source_index = self._act_proxy_model.mapToSource(index)
        act_id = self._act_model.record(source_index.row()).value('id')
        self._proj_cat_model.setFilter('act_id = {}'.format(act_id))
        self._dept_weight_model.setFilter('act_id = {}'.format(act_id))
        self._proj_cat_model.select()
        self._dept_weight_model.select()

    def _on_commit_trigger(self):
        if not any(model.isDirty() for model in self._models):
            QMessageBox.information(self._view, 'Info', 'Nothing to commit!!')
            return

        res = QMessageBox.warning(
            self._view,
            'Confirmation',
            'Do you want to commit changes to the database?',
            QMessageBox.Save | QMessageBox.Cancel
        )
        if res == QMessageBox.Save:
            for model in self._models:
                model.submitAll()

    def _on_revert_trigger(self):
        count = 0
        for model in self._models:
            if model.isDirty():
                model.revertAll()
                count += 1
        if count:
            QMessageBox.information(self._view, 'Info', '{} model(s) reverted'.format(count))

    def _on_close(self):
        if any(model.isDirty() for model in self._models):
            res = QMessageBox.warning(
                self._view,
                'Confirmation',
                "You've changed some values, do you want to commit them?",
                QMessageBox.Yes | QMessageBox.No
            )
            if res == QMessageBox.Yes:
                for model in self._models:
                    model.submitAll()

    def connect_view_models(self):
        self._view.act_tview.setModel(self._act_proxy_model)
        for colx in [0, 6, 9, 10]:
            self._view.act_tview.hideColumn(colx)
        self._view.act_tview.resizeColumnsToContents()

        self._view.proj_cat_tview.setModel(self._proj_cat_model)
        self._view.proj_cat_tview.hideColumn(0)
        self._view.proj_cat_tview.resizeColumnsToContents()

        self._view.dept_weight_tview.setModel(self._dept_weight_model)
        self._view.dept_weight_tview.hideColumn(0)
        self._view.dept_weight_tview.resizeColumnsToContents()

    def connect_signals(self):
        self._view.filter_ledit.textChanged.connect(
            lambda term: self._act_proxy_model.setFilterFixedString(term)
        )
        self._view.act_tview.selectionModel().currentRowChanged.connect(
            lambda index, _: self._filter_detail_models(index)
        )
        self._view.commit_act.triggered.connect(self._on_commit_trigger)
        self._view.revert_act.triggered.connect(self._on_revert_trigger)
        self._view.close_signal.connect(self._on_close)


class CredentialsController:
    def __init__(self):
        self.credentials = None

    def get_credentials(self, dialog_parent=None):
        dialog = CredentialsView(self, dialog_parent)
        dialog.show()
        if dialog.exec_():
            return self.credentials

    @staticmethod
    def verify_credentials(credentials):
        user = db.get_entry(db.select_user, (int(credentials['payroll']),))
        if user and user.is_admin:
            return compare_digest(
                sha256(credentials['password'].encode('utf-8')).hexdigest(), user.password
            )


class ActHistoryController(Controller):
    def connect_signals(self):
        self._view.filter_ledit.textChanged.connect(
            lambda term: self._model.setFilterFixedString(term)
        )

        self._view.sync_btn.clicked.connect(
            lambda: self._model.sourceModel().sync(show_deleted=self._view.show_deleted_cbox.checkState())
        )
        self._view.show_deleted_cbox.stateChanged.connect(
            lambda state: self._model.sourceModel().sync(show_deleted=state)
        )


class MainController(Controller):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._latest_run_results = []

    def _open_act_editor(self):
        credentials = CredentialsController().get_credentials(self._view)
        if credentials:
            run_act_editor_loop(self._view)

    def _get_table_rows(self):
        rows = []
        for rowx in range(self._view.main_twidget.rowCount()):
            local_row = []
            for colx in range(self._view.main_twidget.columnCount()):
                item = self._view.main_twidget.item(rowx, colx)
                if not item:
                    local_row.append('')
                else:
                    val = self._view.main_twidget.item(rowx, colx).text()
                    if val.isdigit():
                        val = int(val)
                    local_row.append(val)

            rows.append(local_row)

        return rows

    def _clear_previous_results(self):
        for rowx in range(self._view.main_twidget.rowCount()):
            self._view.main_twidget.item(rowx, cfg['total_hrs_colx']).setText(None)
            code_col_val = self._view.main_twidget.item(rowx, 0).text()
            if '.' not in code_col_val and not code_col_val.startswith('24'):
                continue

            unit_rate_item = self._view.main_twidget.item(rowx, cfg['unit_rate_colx'])
            if unit_rate_item.text() and \
                    unit_rate_item.background().color().getRgb() != (255, 255, 255, 255):
                unit_rate_item.setText(None)

    def _parse_user_input(self):
        act_rows = []
        dept_rows = []
        dept_codes = db.get_values(db.select_dept_codes)

        for rowx, row in enumerate(self._get_table_rows()):
            row.append(rowx)

            if row[cfg['code_colx']] in dept_codes:
                dept_rows.append(row)

            elif row[cfg['unit_colx']] == 'LOT':
                keys = (row[cfg['nunits_colx']], row[cfg['unit_rate_colx']])

                if row[cfg['code_colx']] == '800.5':
                    if row[cfg['asbuilt_act_req_colx']]:
                        act_rows.append(row)

                elif all(isinstance(val, Number) for val in keys) or \
                        all(not isinstance(val, Number) for val in keys):
                    act_rows.append(row)

            elif isinstance(row[cfg['nunits_colx']], Number):
                act_rows.append(row)

        return act_rows, dept_rows

    def _process_input(self):
        act_rows, dept_rows = self._parse_user_input()
        acts = [make_act(row=r) for r in act_rows]
        ActTotalHrsCalc().calc_n_set(acts)
        act_family_totals = [
            ActFamilyTotal(id_=key, row_indices=val, acts=acts)
            for key, val in cfg['act_family_id_index_map'].items()
        ]
        proj_total = ProjTotal(act_family_totals)
        dept_totals = [DeptShareCalc(r, acts, proj_total.results[0]['value']) for r in dept_rows]
        self._latest_run_results = [acts,  dept_totals, act_family_totals, proj_total]

    def _on_run_trigger(self):
        try:
            self._clear_previous_results()
            self._view.dump_cfg()
            self._process_input()
            acts, dept_totals, family_totals, proj_total = self._latest_run_results
            write_to_twidget(self._view.main_twidget, acts+dept_totals+family_totals+[proj_total])

        except Exception as err:
            err_msg = format_exc().replace('\n', '<br>')
            if isinstance(err, CustomError):
                err_msg = err

            err_view = TextDisplay(
                '<font color=red>{}</font>'.format(err_msg),
                title='ERROR',
                parent=self._view
            )
            err_view.resize(*cfg['err_view_size'])
            err_view.show()

    def _scroll(self, direction):
        rowx = self._view.main_twidget.rowCount()-1 if direction == 'bottom' else 0
        self._view.main_twidget.scrollToItem(
            self._view.main_twidget.item(rowx, cfg['total_hrs_colx'])
        )

    def _on_export_mhrs_trigger(self):
        try:
            dest = QFileDialog.getSaveFileName(
                self._view, caption="New File", filter="Excel (*.xlsx)"
            )[0]
            if not dest:
                return
            output_wb = HyperWorkbook('files/outputtemp.xlsx', read_only=False)
            results = self._latest_run_results[0] + self._latest_run_results[1]
            write_to_xl(output_wb.worksheets[0], results)
            output_wb.save(dest)
            abs_dest = abspath(dest)
            QMessageBox.information(
                self._view, 'DONE', '<a href="file:///{}">{}</a>'.format(abs_dest, abs_dest)
            )

        except:
            from traceback import format_exc
            print(format_exc())

    def _display_proj_cat_desc(self):
        view = TextDisplay(
            cfg['proj_cat_desc'], title='Project Categories', parent=self._view
        )
        view.resize(*cfg['proj_cat_view_size'])
        view.show()

    def _on_enter_press(self):
        self._view.main_twidget.setCurrentCell(
            self._view.main_twidget.currentRow()+1, self._view.main_twidget.currentColumn()
        )

    def _on_del_press(self):
        selected = self._view.main_twidget.selectedItems()
        for item in selected:
            if item.isEditable():
                item.setText(None)

    def _on_del_input_trigger(self):
        for rowx in range(self._view.main_twidget.rowCount()):
            for colx in range(self._view.main_twidget.columnCount()):
                item = self._view.main_twidget.item(rowx, colx)
                if item.isEditable():
                    item.setText(None)
        self._clear_previous_results()

    def connect_signals(self):
        self._view.act_change_history_act.triggered.connect(
            lambda: run_act_change_history_loop(self._view)
        )
        self._view.run_act.triggered.connect(self._on_run_trigger)
        self._view.act_editor_act.triggered.connect(self._open_act_editor)
        self._view.proj_cat_act.triggered.connect(self._display_proj_cat_desc)
        self._view.about_act.triggered.connect(
            lambda: QMessageBox.about(self._view, "APP CARD", cfg['about'])
        )
        self._view.scroll_top_act.triggered.connect(lambda: self._scroll('top'))
        self._view.scroll_bottom_act.triggered.connect(lambda: self._scroll('bottom'))
        self._view.export_mhrs_act.triggered.connect(self._on_export_mhrs_trigger)
        self._view.del_input_act.triggered.connect(self._on_del_input_trigger)

        # keyboard
        self._view.main_twidget.enter_key_pressed.connect(self._on_enter_press)
        self._view.main_twidget.del_key_pressed.connect(self._on_del_press)
        # self._view.main_twidget.paste_key_pressed.connect(self._on_paste_press)


# Runners ##########################################################################################


def _open_qsql_connection():
    qconn = QSqlDatabase.addDatabase('QSQLITE')
    db_name = cfg['prod_db_name'] if cfg['app_mode'] == 'prod' else cfg['dev_db_name']
    qconn.setDatabaseName('files/' + db_name)
    qconn.open()
    return qconn


def run_act_editor_loop(view_parent):
    _open_qsql_connection()
    act_proxy_model = make_act_model()
    proj_cat_model = make_proj_cat_model()
    dept_weight_model = make_dept_weight_model()
    editor_view = ActEditorView(parent=view_parent)
    controller = ActEditorController(
        view=editor_view, models=[act_proxy_model, proj_cat_model, dept_weight_model]
    )
    controller.connect_view_models()
    controller.connect_signals()
    editor_view.show()


def run_act_change_history_loop(view_parent):
    _open_qsql_connection()
    model = make_act_change_history_model()
    dialog = ActHistoryView(parent=view_parent)
    dialog.table_view.resizeColumnsToContents()
    dialog.table_view.setModel(model)
    controller = ActHistoryController(view=dialog, model=model)
    controller.connect_signals()
    dialog.show()
