from abc import ABCMeta, abstractmethod
from contextlib import suppress
from numbers import Number
from math import ceil, fsum

import db
from toolkit import cfg
from toolkit import round_int


# Hrs rate equations ###############################################################################


def make_eq(act_id):
    equations = {
        0: TotalHrsEq0,
        68: TotalHrsEq68,
        28: TotalHrsEq282930,
        29: TotalHrsEq282930,
        30: TotalHrsEq282930
    }
    return equations.get(act_id, TotalHrsEq0)(act_id)


class TotalHrsEq(metaclass=ABCMeta):
    def __init__(self, act_id):
        self.related_act_id = act_id
        self._codes = cfg['eq{}_codes'.format(self.related_act_id)]
        self._factor = cfg['eq{}_factor'.format(self.related_act_id)]

    def _get_related_acts(self, acts):
        return [act for act in acts if any(act.code.startswith(code) for code in self._codes)]

    @abstractmethod
    def calc_act_total_hrs(self, *args, **kwargs):
        pass


class TotalHrsEq0(TotalHrsEq):
    def calc_act_total_hrs(self, *args, **kwargs):
        total_hrs = fsum(act.total_hrs for act in self._get_related_acts(*args, **kwargs))
        return self._factor * total_hrs


class TotalHrsEq282930(TotalHrsEq):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ignore = cfg['eq{}_ignore'.format(self.related_act_id)]

    def _get_related_acts(self, acts):
        return [
            act for act in acts if any(
                act.code.startswith(code) and act.code not in self._ignore for code in self._codes
            )
        ]

    def calc_act_total_hrs(self, *args, **kwargs):
        related_acts = self._get_related_acts(*args, **kwargs)
        total_hrs = 0
        for parent_code, factor in zip(self._codes, self._factor):
            for act in related_acts:
                if act.code.startswith(parent_code):
                    total_hrs += factor * act.total_hrs
        return total_hrs


class TotalHrsEq68(TotalHrsEq):
    def calc_act_total_hrs(self, *args, **kwargs):
        acts = self._get_related_acts(*args, **kwargs)
        if not acts:
            return 0

        # 6 * 120.1  +  3 * the rest summation or 3 * all summation provided that 120.1 is not included
        if acts[0].code == '120.1':
            return self._factor * ( 2 * acts[0].units_count + fsum(act.units_count for act in acts[1:]) )
        return self._factor * fsum(act.units_count for act in acts)


# Act total hrs calculator #########################################################################


class CustomError(Exception):
    pass


class EntryError(CustomError):
    pass


class ActTotalHrsCalc:
    @staticmethod
    def _calc_std(act):
        return act.unit_rate * act.get_nunits()

    @staticmethod
    def _calc_cp_or_mirror_sensitive(act):
        cp_nunits, mirror_nunits = [val or 0 for val in act.get_cp_mirror_nunits()]
        original_nunits = act.get_nunits() - cp_nunits - mirror_nunits
        if (cp_nunits+mirror_nunits) >= act.get_nunits():
            raise EntryError(cfg['cp_err_msg'].format(act.code))

        units = (cp_nunits * act.copy_factor) + \
                (mirror_nunits * act.mirror_factor) + \
                original_nunits
        return act.unit_rate * units

    def _calc_precedence_1(self, act):
        if act.is_cp_or_mirror_sensitive():
            return self._calc_cp_or_mirror_sensitive(act)
        return self._calc_std(act)

    @staticmethod
    def _calc_precedence_other(id_, acts):
        return make_eq(id_).calc_act_total_hrs(acts)

    def calc_n_set(self, acts):
        # calc_precedence > 1 identifies activities with total hrs equations
        # as well as identifying its order in calculation process
        # the following loop makes sure that the high precedence acts get calculated before
        # low ones

        for precedence in sorted(db.get_values(db.select_calc_precedence)):
            same_precedence_acts = [act for act in acts if act.calc_precedence == precedence]

            for act in same_precedence_acts:
                if precedence == 1:
                    act.total_hrs = ceil(self._calc_precedence_1(act))
                else:
                    # act.get_proj_cat_factor() added to fix 620.1, currently it has no effect on
                    # any other act, during any modification for act with precedence other than 1
                    # this MUST be checked again for side effect.
                    act.total_hrs = ceil(self._calc_precedence_other(act.id, acts)) * \
                                    act.get_proj_cat_factor()


# Activities #######################################################################################


def make_act(row):
    entry = db.get_entry(db.select_act, (row[cfg['code_colx']],))
    if entry.has_parts:
        return PartitionedActivity(entry, row)
    return StdActivity(entry, row)


class Writable(metaclass=ABCMeta):
    """defines a common interface for the writing func"""
    @property
    @abstractmethod
    def results(self):
        pass


class Activity(Writable, metaclass=ABCMeta):
    def __init__(self, entry, row):
        self._row = row
        self._db_entry = entry
        self._rowx = row[-1]
        self.units_count = self._row[cfg['nunits_colx']]
        self.unit_rate = self._get_unit_rate()
        self.total_hrs = 0

    def __getattr__(self, name):
        return getattr(self._db_entry, name)

    @abstractmethod
    def _get_unit_rate(self):
        pass

    @property
    def results(self):
        # unit rate is an emtpy string if no value
        unit_rate = self.unit_rate
        with suppress(TypeError):
            unit_rate = ceil(self.unit_rate)
        output = [
            {'rowx': self._rowx, 'colx': cfg['total_hrs_colx'], 'value': ceil(self.total_hrs)},
            {'rowx': self._rowx, 'colx': cfg['unit_rate_colx'], 'value': unit_rate}
        ]

        if self._row[cfg['unit_colx']] != 'lot' and self.get_nunits():
            output.append(
                {'rowx': self._rowx, 'colx': cfg['nunits_colx'], 'value': self.get_nunits()}
            )
        return output

    def get_proj_cat_factor(self):
        # proj_cat comes from the UI and stored in user cfg not app cfg, check reason not clear
        if self.proj_cat_sensitive and cfg['proj_cat']:
            proj_cat_id = db.get_value(db.select_proj_cat_id, (cfg['proj_cat'],))
            return db.get_value(db.select_proj_cat_factor, (self.id, proj_cat_id))
        return 1

    def get_nunits(self):
        return self._row[cfg['nunits_colx']]

    def get_cp_mirror_nunits(self):
        return self._row[cfg['cp_colx']], self._row[cfg['mirror_colx']]

    def is_cp_or_mirror_sensitive(self):
        # if no number in cp or mirror columns
        if isinstance(self, PartitionedActivity) or not any(
                isinstance(val, Number)
                for val in (self._row[cfg['cp_colx']], self._row[cfg['mirror_colx']])
        ):
            return False
        return True


class StdActivity(Activity):
    def _get_unit_rate(self):
        rate = self._db_entry.unit_rate or self._row[cfg['unit_rate_colx']]
        return rate * self.get_proj_cat_factor()


class PartitionedActivity(Activity):
    def _get_parts_weight(self):
        part_weights = []
        for id_, colx in enumerate([cfg[key] for key in cfg['part_keys']], start=1):
            if self._row[colx]:
                part_weights.append(
                    db.get_value(db.select_part_weight, (id_,))
                )

        return fsum(part_weights)

    def _get_unit_rate(self):
        return self._get_parts_weight() * self._db_entry.unit_rate * self.get_proj_cat_factor()


# Totals ###########################################################################################


class ActFamilyTotal(Writable):
    def __init__(self, id_=None, row_indices=None, acts=None):
        super().__init__()
        self._id = id_
        self._indices = row_indices
        self._total_hrs = fsum(act.total_hrs for act in acts if act.code.startswith(self._id))

    @property
    def results(self):
        return [
            {
                'rowx': rowx,
                'colx': cfg['total_hrs_colx'],
                'value': round_int(self._total_hrs, denom=10, only_up=True)
            }
            for rowx in self._indices
        ]


class DeptShareCalc(Writable):
    def __init__(self, row, acts, proj_total):
        self._rowx = row[-1]
        self._dept_id = db.get_value(db.select_dept_id, (row[cfg['code_colx']],))
        self._acts = acts
        self._proj_total = proj_total
        self._asbuilt_gt = self._calc_gt_for_asbuilt()
        self._share_percent, self._share_hrs = self._calc_share() or [None, None]

    @property
    def results(self):
        return [
            {'rowx': self._rowx, 'colx': cfg['total_hrs_colx'], 'value': round_int(self._share_hrs)},
            {'rowx': self._rowx, 'colx': cfg['unit_rate_colx'], 'value': self._share_percent},
        ]

    @staticmethod
    def _is_asbuilt_act(act):
        return True if act.code == cfg['asbuilt_act_code'] else False

    def _calc_gt_for_asbuilt(self):
        gt = 0
        for code_ls in cfg['dept_asbuilt_map'].values():
            for code in code_ls:
                for act in self._acts:
                    if act.code.startswith(code):
                        gt += act.total_hrs
        return gt

    def _get_dept_weight_for_asbuilt_act(self):
        # if this is the asbuilt activity and it's required
        # get activity codes corresponding to dept_id
        # calc total hrs for the above retrieved acts
        # calc gt for the 3 depts activities
        #  return total hrs calculated above / gt
        act_codes = cfg['dept_asbuilt_map'].get(str(self._dept_id))
        if not act_codes:  # not one of the 3 depts concerned with asbuilt
            return 0

        total_hrs = fsum(
            act.total_hrs for code in act_codes for act in self._acts if act.code.startswith(code)
        )
        if self._asbuilt_gt:
            return round(total_hrs / self._asbuilt_gt, 2)

    def _calc_share(self):
        total_hrs = 0

        for act in self._acts:
            dept_act_weight = self._get_dept_weight_for_asbuilt_act() \
                if self._is_asbuilt_act(act) \
                else db.get_value(db.select_dept_act_weight, (self._dept_id, act.id))
            total_hrs += act.total_hrs * dept_act_weight

        if self._proj_total:
            return '{}%'.format(round(total_hrs * 100 / self._proj_total, 1)), total_hrs


class ProjTotal(Writable):
    def __init__(self, family_totals):
        self._family_totals = family_totals
        self._proj_total = fsum(ftotal.results[1]['value'] for ftotal in family_totals)

    @property
    def results(self):
        return [
            {
                'rowx': cfg['proj_total_rowx'],
                'colx': cfg['total_hrs_colx'],
                'value': round_int(self._proj_total, denom=10, only_up=True)
             }
        ]


# Writers ##########################################################################################


def write_to_xl(ws, objects):
    # writing project info
    for cell, value in zip(cfg['proj_info_cell_keys'], cfg['proj_info_value_keys']):
        ws[cfg[cell]].value = cfg[value]

    for obj in objects:
        for result in obj.results:
            value = result['value']
            if not value:
                continue
            ws.cell(
                row=result['rowx'] + cfg['xl_row_offset'],
                column=result['colx'] + cfg['xl_col_offset']
            ).value = value


def write_to_twidget(twidget, objects):
    for obj in objects:
        for result in obj.results:
            val = result['value']
            if not val:
                continue
            twidget.item(result['rowx'], result['colx']).setText(str(val))
