from collections import namedtuple


conn = None
cursor = None

select_content = 'SELECT * FROM MainViewContent'
update_act_attr = 'UPDATE Activities SET {} = ? WHERE id = ?'
update_act_proj_cat_factor = 'UPDATE ActProjCatFactor SET factor = ? ' \
                              'WHERE act_id = ? and proj_cat_id = ?'
insert_change = 'INSERT INTO ActHistory ' \
                '(act_id, changed_table, key, changed_attr, prev_val, new_val) ' \
                'VALUES (:act_id, :changed_table, :key, :changed_attr, :prev_val, :new_val)'
select_user = 'SELECT * FROM Users WHERE payroll = ?'
select_all_acts = 'SELECT * FROM Activities;'
select_all_changes = 'SELECT * FROM ActHistoryView'
select_proj_cat_factors = 'SELECT factor FROM ActProjCatFactor WHERE act_id = ? and proj_cat_id < 4;'
select_act_parent_dsc = 'SELECT desc FROM ActParents WHERE id = ?;'
select_act = 'SELECT * FROM Activities WHERE code = ?;'
select_part_weight = 'SELECT weight FROM ActParts WHERE id = ?;'
select_proj_cat_id = 'SELECT id FROM ProjCat WHERE cat = ?;'
select_proj_cat_factor = 'SELECT factor FROM ActProjCatFactor WHERE act_id = ? and proj_cat_id = ?;'
select_calc_precedence = 'SELECT DISTINCT calc_precedence FROM Activities;'
select_dept_codes = 'SELECT code FROM Departments;'
select_dept_id = 'SELECT id FROM Departments WHERE code = ?;'
select_dept_act_weight = 'SELECT weight FROM ActDeptWeights WHERE dept_id = ? and act_id = ?;'


def _make_entry(cur, entries):
    Entry = namedtuple('Entry', [d[0] for d in cur.description])
    return list(map(Entry._make, entries))


def get_value(statement, args=()):
    return cursor.execute(statement, args).fetchone()[0]


def get_values(statement, args=(), valx=0):
    return [entry[valx] for entry in cursor.execute(statement, args).fetchall()]


def get_entry(statement, args=()):
    entry = cursor.execute(statement, args).fetchone()
    if entry:
        return _make_entry(cursor, [entry])[0]


def get_entries(statement, args=(), as_ntuples=True):
    entries = cursor.execute(statement, args).fetchall()
    if as_ntuples:
        return _make_entry(cursor, entries)
    return [[tup[0] for tup in cursor.description]] + entries


def commit(statement, args=()):
    ret = cursor.execute(statement, args)
    conn.commit()
    return ret.rowcount
