import sys
import sqlite3 as sql

from PyQt5.QtWidgets import *

from views import MainView
from controllers import MainController
import db
from toolkit import cfg


cfg.init('MHrsEstimator')
app = QApplication(sys.argv)
db_name = cfg['prod_db_name'] if cfg['app_mode'] == 'prod' else cfg['dev_db_name']
db.conn = sql.connect('files/' + db_name)
db.cursor = db.conn.cursor()

main_view = MainView()
main_ctl = MainController(main_view)
main_view.init_table_template(db.get_entries(db.select_content, as_ntuples=False)[1:])
cfg.handle_views([main_view])    # this will fill previous user input if any
main_ctl.connect_signals()
main_view.show()

app.exec_()
cfg.save()
db.cursor.close()
db.conn.close()
