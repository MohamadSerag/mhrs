from PyQt5.QtCore import QSortFilterProxyModel, Qt
from PyQt5.QtSql import QSqlQueryModel, QSqlRelationalTableModel, QSqlTableModel, \
    QSqlRelation

import db
from toolkit import cfg


class ActChangeHistoryModel(QSqlQueryModel):
    def sync(self, show_deleted=False):
        if show_deleted:
            self.setQuery(cfg['act_history_model_q_all'])
        else:
            self.setQuery(cfg['act_history_model_q'])


class ActChangeHistoryProxyModel(QSortFilterProxyModel):
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Vertical:
            return section + 1
        return super().headerData(section, orientation, role)

    def filterAcceptsRow(self, rowx, source_parent):
        cell_values = [
            self.sourceModel().data(self.sourceModel().index(rowx, colx))
            for colx in range(self.sourceModel().columnCount())
        ]
        return any(self.filterRegExp().pattern() in str(val).lower() for val in cell_values)

    def data(self, modelx, role=Qt.DisplayRole):
        if role == Qt.TextAlignmentRole:
            return Qt.AlignCenter
        return super().data(modelx, role)


class ActProxyModel(QSortFilterProxyModel):
    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if role == Qt.DisplayRole and orientation == Qt.Vertical:
            return section + 1
        return super().headerData(section, orientation, role)

    def filterAcceptsRow(self, rowx, source_parent):
        cell_values = [
            self.sourceModel().data(self.sourceModel().index(rowx, colx))
            for colx in range(self.sourceModel().columnCount())
        ]
        return any(self.filterRegExp().pattern() in str(val).lower() for val in cell_values)

    def data(self, modelx, role=Qt.DisplayRole):
        if role == Qt.TextAlignmentRole:
            if modelx.column() in [1, 3]:
                return Qt.AlignLeft | Qt.AlignVCenter
            return Qt.AlignCenter

        return super().data(modelx, role)

    def lessThan(self, modelx1, modelx2):
        code1, sub1 = self.sourceModel().data(modelx1).split('.')
        code2, sub2 = self.sourceModel().data(modelx2).split('.')

        code_res = int(code1) < int(code2)
        sub_res = int(sub1) <= int(sub2)
        if code1 == '750' or code2 == '750':
            return code_res
        return code_res and sub_res


class ActChangesRecorder:
    def __init__(self):
        # {record_id: {record_data}, record_id: {record_data}}
        self.changes = {}

    def record(self, modelx, new_val):
        # the role of the record_id is to replace
        # the prev. change of the same act with the newer one
        act_id = modelx.model().record(modelx.row()).value(0)
        record_id = (modelx.row(), modelx.column())
        key = ''

        if isinstance(modelx.model(), DetailTableModel):
            code = modelx.model().record(modelx.row()).value(1)
            act_id = db.get_entry(db.select_act, (code,)).id
            record_id = '{}-{}'.format(
                modelx.row(), modelx.model().record(modelx.row()).value(1)
            )
            key = modelx.model().record(modelx.row()).value(2)

        record_data = {
            'act_id': act_id,
            'changed_table': modelx.model().tableName(),
            'key': key,
            'changed_attr': modelx.model().headerData(
                modelx.column(), Qt.Horizontal, Qt.DisplayRole
            ),
            'prev_val': modelx.data(Qt.DisplayRole),
            'new_val': new_val
        }
        self.changes[record_id] = record_data

    def submit_all(self):
        for change in self.changes.values():
            db.commit(db.insert_change, change)
        self.changes = {}


class CustomTableModel(QSqlRelationalTableModel):
    _recorder = ActChangesRecorder()

    def setData(self, modelx, new_value, role=Qt.EditRole):
        self._recorder.record(modelx, new_value)
        return super().setData(modelx, new_value, role)

    def submitAll(self):
        self._recorder.submit_all()
        return super().submitAll()

    def revertAll(self):
        self._recorder.changes.clear()
        return super().revertAll()


class ActTableModel(CustomTableModel):
    def flags(self, modelx):
        if modelx.column() not in cfg['editable_colx']['act_model'] \
                or modelx.data(Qt.DisplayRole) == 'N/A':
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable
        else:
            return super().flags(modelx)

    def data(self, modelx, role=Qt.DisplayRole):
        data = super().data(modelx, role)
        if role != Qt.DisplayRole:
            return data

        if modelx.column() in cfg['editable_colx']['act_model']:
            if (modelx.column() == 5 and data == 0) or (modelx.column() != 5 and data == 1):
                return 'N/A'
        return data


class DetailTableModel(CustomTableModel):
    def data(self, modelx, role=Qt.DisplayRole):
        if role == Qt.TextAlignmentRole and \
                        modelx.column() in cfg['editable_colx']['detail_models']:
            return Qt.AlignCenter
        return super().data(modelx, role)

    def flags(self, modelx):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable \
            if modelx.column() in cfg['editable_colx']['detail_models'] \
            else Qt.ItemIsSelectable | Qt.ItemIsEnabled


def make_act_model():
    try:

        model = ActTableModel()
        model.setTable('Activities')
        model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        model.setRelation(1, QSqlRelation('ActParents', 'id', 'desc'))

        for colx, title in zip(*cfg['act_model_header_titles_n_indexes']):
            model.setHeaderData(colx, Qt.Horizontal, title)

        model.select()
        proxy = ActProxyModel()
        proxy.setSourceModel(model)
        proxy.sort(2)

    except:
        from traceback import format_exc
        print(format_exc())
    return proxy


def make_proj_cat_model():
    model = DetailTableModel()
    model.setTable('ActProjCatFactor')
    model.setEditStrategy(QSqlTableModel.OnManualSubmit)
    model.setRelation(1, QSqlRelation('Activities', 'id', 'code'))
    model.setRelation(2, QSqlRelation('ProjCat', 'id', 'cat'))
    model.setHeaderData(1, Qt.Horizontal, 'Activity Code')
    model.setHeaderData(2, Qt.Horizontal, 'Project Category')
    model.setHeaderData(3, Qt.Horizontal, 'Category Factor')
    return model


def make_dept_weight_model():
    model = DetailTableModel()
    model.setTable('ActDeptWeights')
    model.setEditStrategy(QSqlTableModel.OnManualSubmit)
    model.setRelation(1, QSqlRelation('Activities', 'id', 'code'))
    model.setRelation(2, QSqlRelation('Departments', 'id', 'name'))
    model.setHeaderData(1, Qt.Horizontal, 'Activity Code')
    model.setHeaderData(2, Qt.Horizontal, 'Department')
    model.setHeaderData(3, Qt.Horizontal, 'Weight')
    return model


def make_act_change_history_model():
    model = ActChangeHistoryModel()
    model.setQuery(cfg['act_history_model_q'])
    proxy = ActChangeHistoryProxyModel()
    proxy.setSourceModel(model)
    return proxy
