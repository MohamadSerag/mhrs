import sys

from cx_Freeze import setup, Executable


def build():
    sys.path.append('.')

    base = None
    if sys.platform == 'win32':
        base = 'Win32GUI'

    includefiles = [
        'files'
        # r'C:\Users\13355\AppData\Local\Programs\Python\Python36-32\Lib\site-packages\PyQt5\Qt\bin\libEGL.dll',
        # r'C:\Users\13355\AppData\Local\Programs\Python\Python36-32\Lib\site-packages\PyQt5\Qt\plugins\sqldrivers'
    ]
    options = {
        'build_exe': {
            'includes': 'atexit',
            'include_files': includefiles,
            'zip_include_packages': ["*"],
            "zip_exclude_packages": []
        }
    }
    executables = [
        Executable('main.py', base=base, icon='files/img/app.ico', targetName='MHrsEstimator.exe')
    ]

    setup(name='RUN',
          version='1.0',
          description='RUN',
          options=options,
          executables=executables
          )


def change_app_mode(old, new):
    newtext = open('files/config.json').read().replace(old, new)
    with open('files/config.json', 'w') as f:
        f.write(newtext)


# change_app_mode('"dev"', '"prod"')
build()
# change_app_mode('"prod"', '"dev"')
