import os
import subprocess as sub


os.chdir('..')
sub.call(
    ['pyrcc5', 'files/img/icons.qrc', '-o', 'icons_rc.py']
)
sub.call(['pyuic5', '-x', 'views/main.ui', '-o', 'views/main_view.py'])
sub.call(['pyuic5', '-x', 'views/act_editor.ui', '-o', 'views/act_editor_view.py'])
sub.call(['pyuic5', '-x', 'views/credentials.ui', '-o', 'views/credentials_view.py'])
sub.call(['pyuic5', '-x', 'views/act_history.ui', '-o', 'views/act_history_view.py'])
