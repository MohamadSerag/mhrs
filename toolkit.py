import os
import json
from pathlib import Path
from collections import ChainMap
from contextlib import suppress
import warnings

import openpyxl as xl


# Application configuration #########################################################################

class _Config(ChainMap):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._user_cfg_path = None
        self._app_cfg_path = None

    def init(self, app_name, app_cfg_path='files/config.json'):
        self._user_cfg_path = Path(os.environ.get('APPDATA', '/private/tmp'), app_name,
                                   'config.json')
        self._app_cfg_path = Path(app_cfg_path)
        self._load_file(self._app_cfg_path, at_end=True)

    def _load_file(self, path, at_end=False):
        """
        I want to insert user configs at first position, so they override others and
        gets saved to user's appdata.
        ui's dump_cfg doesn't create a new map, it only updates the one in first position
        """
        with suppress(FileNotFoundError):
            with path.open() as file:
                index = len(self.maps) if at_end else 0
                self.maps.insert(index, json.load(file))
                return True

    def handle_views(self, views):
        if self._load_file(self._user_cfg_path):
            # in this case, there're 2 filled maps (user/app) and one empty in th middle
            for view in views:
                view.load_cfg()
        else:
            # this will update the first map, which in this case is initially empty
            for view in views:
                view.dump_cfg()

    def save(self, map_i=0):
        if not self._user_cfg_path.parent.exists():
            os.mkdir(str(self._user_cfg_path.parent))

        with self._user_cfg_path.open(mode='w') as f:
            json.dump(self.maps[map_i], f, indent=2)


cfg = _Config()


# Excel #############################################################################################

class HyperWorkbook:
    def __init__(self, path, read_only=True, *args, **kwargs):
        warnings.simplefilter('ignore')
        self._file = open(path, mode='rb') if read_only else path
        self._wb = xl.load_workbook(self._file, read_only=read_only, *args, **kwargs)

    @staticmethod
    def _get_rows(ws, ignore_empty=True, lower_case=True, upper_case=False, include_indexes=False):
        for rowx, row in enumerate(ws.rows):
            row_values = []

            for cell in row:
                val = cell.value
                with suppress(AttributeError):
                    val = val.strip()
                    if lower_case:
                        val = val.lower()
                    elif upper_case:
                        val = val.upper()

                row_values.append(val)

            if ignore_empty:
                # any returns false if no value is true, so it's if any() == false
                if not any(v for v in row_values):
                    continue

            ret = [rowx] + row_values if include_indexes else row_values
            yield ret

    def get_sheet_rows(self, index=0, *args, **kwargs):
        # return the generator obj
        return self._get_rows(self._wb.worksheets[index], *args, **kwargs)

    def get_all_rows(self, *args, **kwargs):
        for ws in self._wb.worksheets:
            yield list(self._get_rows(ws=ws, *args, **kwargs))

    def close(self):
        self._file.close()

    def __getattr__(self, name):
        return getattr(self._wb, name)


# Miscellaneous #####################################################################################

def round_int(val, denom=5, only_up=False):
    val = round(val)
    down_val = val % denom
    up_val = denom - down_val
    # if no down_val (remainder) then no rounding required
    if not down_val:
        return val
    if only_up:
        return val + up_val

    return val + up_val if up_val <= down_val else val - down_val
