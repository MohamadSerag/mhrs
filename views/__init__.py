from contextlib import suppress

from PyQt5.QtCore import Qt, QSize, pyqtSignal, QDate
from PyQt5.QtGui import QBrush, QColor, QFont, QKeySequence, QIcon, QPixmap
from PyQt5.QtWidgets import QMainWindow, QDialog, QTableWidgetItem, QToolBar, QAction, \
    QTableWidget, QTextEdit, QVBoxLayout, QLineEdit, QStyledItemDelegate, QComboBox, QDoubleSpinBox, \
    QLabel

from .main_view import Ui_main_ui as MainWindow
from .act_editor_view import Ui_MainWindow as ActEditorWindow
from .credentials_view import Ui_Dialog as CredentialsDialog
from .act_history_view import Ui_Dialog as ActHistoryDialog
from toolkit import cfg


class View:
    def __init__(self, *args, **kwargs):
        self.setAttribute(Qt.WA_DeleteOnClose)
        if kwargs.keys():
            self.setupUi(self, **kwargs)
        else:
            self.setupUi(self)


class CredentialsView(QDialog, View, CredentialsDialog):
    def __init__(self, ctl, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._controller = ctl

    def accept(self):
        if not self.payroll_ledit.text() or not self.password_ledit.text():
            self.msg_label.setText('<font color=maroon>Invalid credentials</font>')

        else:
            credentials = {
                'payroll': self.payroll_ledit.text(), 'password': self.password_ledit.text()
            }
            if not self._controller.verify_credentials(credentials):
                self.msg_label.setText('<font color=maroon>Invalid credentials</font>')

            else:
                self._controller.credentials = credentials
                super().accept()


class ActHistoryView(QDialog, View, ActHistoryDialog):
    """
    dialog emits finished signal on closing which is connect to a method in the controller
    hence, no overriding here
    """
    pass


class TextDisplay(QDialog):
    def __init__(self, text, title='', parent=None):
        super().__init__(parent)
        self.text_edit = QTextEdit(text)
        self.setWindowTitle(title)
        self.text_edit.setReadOnly(True)
        layout = QVBoxLayout()
        layout.addWidget(self.text_edit)
        self.setLayout(layout)
        self.setAttribute(Qt.WA_DeleteOnClose)
        self.setWindowFlags(self.windowFlags() | Qt.WindowMinMaxButtonsHint)


# Item delegates ###################################################################################


class ItemDelegate(QStyledItemDelegate):
    @staticmethod
    def _make_ledit(parent):
        ledit = QLineEdit(parent)
        ledit.setInputMask('0000000')
        return ledit

    @staticmethod
    def _make_cbox(parent):
        cbox = QComboBox(parent=parent)
        cbox.insertItem(0, '')
        cbox.insertItem(1, 'Yes')
        return cbox

    @staticmethod
    def _make_double_sbox(parent, max_val=1.0, min_val=0, dec=1):
        sbox = QDoubleSpinBox(parent=parent)
        sbox.setMaximum(max_val)
        sbox.setMinimum(min_val)
        sbox.setDecimals(dec)
        return sbox


class MainTableItemDelegate(ItemDelegate):
    def createEditor(self, parent, options, index):
        rowx_range = range(cfg['mr_start_rowx'], cfg['mr_end_rowx'])
        colx_range = range(cfg['mrq_colx'], cfg['vpr_colx']+1)
        rowx, colx = index.row(), index.column()

        if (rowx in rowx_range and colx in colx_range) or \
           (rowx == cfg['asbuilt_act_req_rowx'] and colx == cfg['asbuilt_act_req_colx']):
            return self._make_cbox(parent)
        return self._make_ledit(parent)


class MasterModelItemDelegate(ItemDelegate):
    def createEditor(self, parent, options, modelx):
        if modelx.column() == 5:
            return self._make_ledit(parent)
        return self._make_double_sbox(parent=parent, max_val=0.9)


class DetailsModelItemDelegate(ItemDelegate):
    def __init__(self, max_val=1.0, min_val=0, dec=1, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._box_attrs = {'max_val': max_val, 'min_val': min_val, 'dec': dec}

    def createEditor(self, parent, options, index):
        return self._make_double_sbox(parent, **self._box_attrs)


# Activity editor ##################################################################################


class ActEditorView(QMainWindow, View, ActEditorWindow):
    """
    couldn't find a signal emitted on close, hence, the closeEvent override
    """
    close_signal = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.act_tview.setItemDelegate(MasterModelItemDelegate())
        self.proj_cat_tview.setItemDelegate(DetailsModelItemDelegate())
        self.dept_weight_tview.setItemDelegate(DetailsModelItemDelegate(dec=2))

    def closeEvent(self, event):
        self.close_signal.emit()
        super().closeEvent(event)


# Main view ########################################################################################


class _CustomTableWidget(QTableWidget):
    enter_key_pressed = pyqtSignal()
    del_key_pressed = pyqtSignal()
    paste_key_pressed = pyqtSignal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._event_signal_map = {
            Qt.Key_Return: self.enter_key_pressed,
            Qt.Key_Enter: self.enter_key_pressed,
            Qt.Key_Delete: self.del_key_pressed,
        }
        self.setItemDelegate(MainTableItemDelegate())

    def keyPressEvent(self, event, *args, **kwargs):
        signal = self._event_signal_map.get(event.key())
        if signal:
            signal.emit()
        elif event.matches(QKeySequence.Paste):
            self.paste_key_pressed.emit()
        else:
            super().keyPressEvent(event, *args, **kwargs)


class _CustomTableWidgetItem(QTableWidgetItem):
    def isEditable(self):
        return self.data(Qt.UserRole) == cfg['write_access_modifier']


class _ItemFormatMgr:
    def __init__(self):
        self._flags_map = {
            "enabled": Qt.ItemIsEnabled,
            "selectable": Qt.ItemIsSelectable,
            "editable": Qt.ItemIsEditable
        }
        self._align_map = {"center": Qt.AlignCenter}
        self._font_map = {"bold": QFont.Bold}
        self._fmt_attr_method_map = {
            'flags': self._format_flags,
            'bg_color': self._format_bg_color,
            'fg_color': self._format_fg_color,
            'font': self._format_font,
            'alignment': self._format_alignment,
        }

    def _format_flags(self, fmt, item):
        if len(fmt["flags"]) == 2:
            item.setFlags(self._flags_map[fmt['flags'][0]] | self._flags_map[fmt['flags'][1]])

        elif len(fmt["flags"]) == 3:
            item.setFlags(
                self._flags_map[fmt['flags'][0]] |
                self._flags_map[fmt['flags'][1]] |
                self._flags_map[fmt['flags'][2]]
            )

    def _format_font(self, fmt, item):
        font = item.font()
        font.setWeight(self._font_map[fmt['font']])
        item.setFont(font)

    def _format_alignment(self, fmt, item):
        item.setTextAlignment(self._align_map[fmt['alignment']])

    @staticmethod
    def _format_span(parent, row, rowx):
        span = 8 if len(row[0].text()) != 4 else 7
        for x, c in enumerate(row[2:]):
            if c.text():
                span = x + 1
                break
        if span > 1:
            parent.setSpan(rowx, 1, 1, span)
        parent.setColumnWidth(1, 325)

    @staticmethod
    def _format_bg_color(fmt, item):
        item.setBackground(QBrush(QColor.fromRgb(*fmt['bg_color'])))

    @staticmethod
    def _format_fg_color(fmt, item):
        item.setForeground(QBrush(QColor.fromRgb(*fmt['fg_color'])))

    @staticmethod
    def _get_item_type(row, item):
        if 'TOTAL' in row[1].text():
            return 'total'

        if row.index(item) == 1:
            if '.' not in row[0].text():
                return 'header_desc'
            return 'desc'

        if row[0].text() and '.' not in row[0].text():
            return 'header'

        elif row[0].text() and '.' in row[0].text():
            if item.text().startswith(cfg['write_access_modifier']):
                return 'editable'

            pre_item = row[row.index(item)-1]
            if pre_item.text() == 'LOT':
                return 'lot'

        return 'default'

    def format_row(self, parent, row, rowx):
        for item in row:
            item_type = self._get_item_type(row, item)
            fmt = cfg['format_map'][item_type]

            # editable specials
            if item_type == 'editable':
                if item.text().split('|')[1]:
                    item.setToolTip(item.text().split('|')[1])
                    item.setIcon(QIcon('files/img/star.png'))
                item.setText('')

            # span
            if fmt.get('span'):
                self._format_span(parent, row, rowx)

            # others
            for fmt_attr in fmt.keys():
                with suppress(KeyError):
                    self._fmt_attr_method_map[fmt_attr](fmt, item)


class MainView(QMainWindow, View, MainWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(custom_widget=_CustomTableWidget, *args, **kwargs)
        self._add_logo()
        self._add_toolbar()
        self.report_rev_date_dedit.setDate(QDate.currentDate())
        self.showMaximized()
        self._formatter = _ItemFormatMgr()

    def _add_logo(self, imgpath='files/img/enppi.png', maxsize=(60, 20)):
        self.status_bar.addWidget(QLabel(''))
        self._enppi_label = QLabel()
        self._enppi_label.setPixmap(QPixmap(imgpath))
        self._enppi_label.setScaledContents(True)
        self._enppi_label.setMaximumSize(*maxsize)
        self.status_bar.addWidget(self._enppi_label)

    def _add_toolbar(self):
        # todo: move size to config which requires config modification
        self.toolbar = QToolBar('Main')
        self.toolbar.setIconSize(QSize(28, 28))
        for action in self.findChildren(QAction):
            if action.whatsThis():
                self.toolbar.addAction(action)
                self.toolbar.addSeparator()

        self.addToolBar(Qt.RightToolBarArea, self.toolbar)

    def init_table_template(self, data):
        """
        filling the empty table view with template data and formats
        """
        self.main_twidget.setRowCount(len(data))
        self.main_twidget.setColumnCount(len(cfg['table_header']))
        self.main_twidget.setHorizontalHeaderLabels(cfg['table_header'])

        for rowx, row in enumerate(data):
            local_row = []
            for colx, value in enumerate(row):
                table_item = _CustomTableWidgetItem(value)
                if value == cfg['write_access_modifier']:
                    table_item.setData(Qt.UserRole, cfg['write_access_modifier'])
                local_row.append(table_item)

            self._formatter.format_row(self.main_twidget, local_row, rowx)
            for colx, item in enumerate(local_row):
                self.main_twidget.setItem(rowx, colx, item)

        self.main_twidget.resizeRowsToContents()

    def dump_cfg(self):
        cfg[self.proj_title_ledit.whatsThis()] = self.proj_title_ledit.text()
        cfg[self.proj_by_ledit.whatsThis()] = self.proj_by_ledit.text()
        cfg[self.report_rev_ledit.whatsThis()] = self.report_rev_ledit.text()
        cfg['report_rev_date'] = self.report_rev_date_dedit.text()

        user_table_input = []
        for rowx in range(self.main_twidget.rowCount()):
            for colx in range(self.main_twidget.columnCount()):
                item = self.main_twidget.item(rowx, colx)
                if item.isEditable() and item.text():
                    user_table_input.append(
                        {'rowx': rowx, 'colx': colx, 'value': item.text()}
                    )
        cfg['user_table_input'] = user_table_input
        cfg[self.proj_cat_cbox.whatsThis()] = self.proj_cat_cbox.currentText()

    def load_cfg(self):
        self.proj_title_ledit.setText(cfg.get(self.proj_title_ledit.whatsThis(), ''))
        self.proj_by_ledit.setText(cfg.get(self.proj_by_ledit.whatsThis(), ''))
        self.report_rev_ledit.setText(cfg.get(self.report_rev_ledit.whatsThis(), ''))
        self.proj_cat_cbox.setCurrentText(cfg.get(self.proj_cat_cbox.whatsThis(), 'A'))

        if not cfg.get('user_table_input'):
            return
        for item_dict in cfg['user_table_input']:
            self.main_twidget.item(item_dict['rowx'], item_dict['colx']).setText(item_dict['value'])
