# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'views/act_history.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(778, 392)
        self.gridLayout = QtWidgets.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.filter_ledit = QtWidgets.QLineEdit(Dialog)
        self.filter_ledit.setClearButtonEnabled(True)
        self.filter_ledit.setObjectName("filter_ledit")
        self.gridLayout.addWidget(self.filter_ledit, 0, 0, 1, 1)
        self.sync_btn = QtWidgets.QPushButton(Dialog)
        self.sync_btn.setMaximumSize(QtCore.QSize(35, 16777215))
        self.sync_btn.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/main/sync.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sync_btn.setIcon(icon)
        self.sync_btn.setIconSize(QtCore.QSize(16, 16))
        self.sync_btn.setFlat(True)
        self.sync_btn.setObjectName("sync_btn")
        self.gridLayout.addWidget(self.sync_btn, 0, 1, 1, 1)
        self.show_deleted_cbox = QtWidgets.QCheckBox(Dialog)
        self.show_deleted_cbox.setObjectName("show_deleted_cbox")
        self.gridLayout.addWidget(self.show_deleted_cbox, 0, 2, 1, 1)
        self.table_view = QtWidgets.QTableView(Dialog)
        self.table_view.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.table_view.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.table_view.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.table_view.setDragDropOverwriteMode(False)
        self.table_view.setAlternatingRowColors(True)
        self.table_view.setSelectionMode(QtWidgets.QAbstractItemView.ExtendedSelection)
        self.table_view.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.table_view.setSortingEnabled(True)
        self.table_view.setObjectName("table_view")
        self.table_view.horizontalHeader().setCascadingSectionResizes(True)
        self.table_view.horizontalHeader().setDefaultSectionSize(105)
        self.table_view.horizontalHeader().setSortIndicatorShown(True)
        self.table_view.horizontalHeader().setStretchLastSection(True)
        self.gridLayout.addWidget(self.table_view, 1, 0, 1, 3)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Activity Change History"))
        self.filter_ledit.setPlaceholderText(_translate("Dialog", "Enter text to filter table"))
        self.sync_btn.setToolTip(_translate("Dialog", "Sync"))
        self.sync_btn.setShortcut(_translate("Dialog", "Enter"))
        self.show_deleted_cbox.setText(_translate("Dialog", "Show Deleted"))

import icons_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

