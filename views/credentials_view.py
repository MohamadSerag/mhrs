# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'credentials.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(243, 122)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.payroll_ledit = QtWidgets.QLineEdit(Dialog)
        self.payroll_ledit.setClearButtonEnabled(True)
        self.payroll_ledit.setObjectName("payroll_ledit")
        self.verticalLayout.addWidget(self.payroll_ledit)
        self.password_ledit = QtWidgets.QLineEdit(Dialog)
        self.password_ledit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.password_ledit.setObjectName("password_ledit")
        self.verticalLayout.addWidget(self.password_ledit)
        self.msg_label = QtWidgets.QLabel(Dialog)
        self.msg_label.setText("")
        self.msg_label.setObjectName("msg_label")
        self.verticalLayout.addWidget(self.msg_label)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Admin Credentials"))
        self.payroll_ledit.setInputMask(_translate("Dialog", "99999"))
        self.payroll_ledit.setPlaceholderText(_translate("Dialog", "Enter Payroll"))
        self.password_ledit.setPlaceholderText(_translate("Dialog", "Enter Password"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

